import React from 'react';
import ReactDOM from 'react-dom';
import FullscreenSearch from './FullscreenSearch';

ReactDOM.render(<FullscreenSearch />, document.getElementById('root'));
